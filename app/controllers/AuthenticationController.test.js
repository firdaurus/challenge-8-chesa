const AuthenticationController = require("./AuthenticationController");
const { User, Role } = require("../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

describe("AuthenticationController", () => {
	describe("authorize", () => {

        let mockRequest, mockResponse, mockNext, authController;

        beforeEach(() => {
            authController = new AuthenticationController({ userModel: null, jwt });

            const mockUser = {
                name: "Admin",
            }

            const mockRole = {
                id: 1,
                name: "ADMIN",
            }

            mockRequest = {
                headers: {
                    authorization: `Bearer ${ authController.createTokenFromUser(mockUser, mockRole)}`,
                },
            }

            mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            mockNext = jest.fn();
        })

		it("should throw an error if the user is not authorized", () => {

            authController.authorize("NOT ADMIN")(mockRequest, mockResponse, mockNext);
            expect(mockResponse.status).toHaveBeenCalledWith(401);
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: expect.objectContaining({
                    message: expect.stringContaining("forbidden"),
                })
            })
        });

		it("should proceed if the user is authorized", () => {

            authController.authorize("ADMIN")(mockRequest, mockResponse, mockNext);
            expect(mockNext).toBeCalled();            
        });
	});

	describe("handleLogin", () => {
		let mockRequest;
		let mockResponse;

		beforeEach(() => {
			mockRequest = {
				body: {
					email: "test@test.com",
					password: "12345",
				},
			};

			mockResponse = {
				status: jest.fn().mockReturnThis(),
				json: jest.fn().mockReturnThis(),
			};
		});

		it("should throw an error if email is not registered", async () => {
			const mockUserModel = {
				findOne: jest.fn().mockReturnValue(null),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
			});

			await mockAuthenticationController.handleLogin(
				mockRequest,
				mockResponse,
				null
			);
			expect(mockResponse.status).toHaveBeenCalledWith(404);
		});

		it("should throw an error if password is wrong", async () => {
			const mockUser = new User({
				email: "test@test.com",
				encryptedPassword: bcrypt.hashSync("wrong password", 10),
			});

			const mockUserModel = {
				findOne: jest.fn().mockReturnValue(mockUser),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
				bcrypt,
			});

			await mockAuthenticationController.handleLogin(
				mockRequest,
				mockResponse,
				null
			);
			expect(mockResponse.status).toHaveBeenCalledWith(401);
		});

		it("should return a token if the user is logged in", async () => {
			const mockUser = new User({
				email: "test@test.com",
				encryptedPassword: bcrypt.hashSync("12345", 10),
				role: "ADMIN",
			});

			const mockUserModel = {
				findOne: jest.fn().mockReturnValue(mockUser),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
				bcrypt,
			});

			mockAuthenticationController.createTokenFromUser = jest
				.fn()
				.mockReturnValue("Some token");

			await mockAuthenticationController.handleLogin(
				mockRequest,
				mockResponse,
				null
			);

			expect(mockResponse.status).toHaveBeenCalledWith(201);
			expect(mockResponse.json).toHaveBeenCalledWith({
				accessToken: expect.any(String),
			});
		});

		it("should show error page if there's caught error", async () => {
			const mockNext = jest.fn();

			const mockAuthenticationController = new AuthenticationController({
				userModel: null,
			});
			await mockAuthenticationController.handleLogin(
				mockRequest,
				mockResponse,
				mockNext
			);
			expect(mockNext).toHaveBeenCalled();
		});
	});

	describe("handleRegister", () => {
		let mockResponse;
		let mockRequest;

		beforeEach(() => {
			mockResponse = {
				status: jest.fn().mockReturnThis(),
				json: jest.fn().mockReturnThis(),
			};

			mockRequest = {
				body: {
					email: "test@test.com",
					name: "test name",
					password: "12345",
				},
			};
		});

		it("should return 422 error if email already exists", async () => {
			const mockUserModel = {
				findOne: jest.fn().mockReturnValue(true),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
			});

			await mockAuthenticationController.handleRegister(
				mockRequest,
				mockResponse,
				null
			);

			expect(mockResponse.status).toHaveBeenCalledWith(422);
			expect(mockResponse.json).toHaveBeenCalled();
		});

		it("should return token if success", async () => {
			const mockUserModel = {
				findOne: jest.fn().mockReturnValue(false),
				create: jest.fn().mockReturnThis(),
			};

			const mockRoleModel = {
				findOne: jest.fn().mockReturnValue({ id: 1 }),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
				roleModel: mockRoleModel,
			});

			mockAuthenticationController.encryptPassword = jest
				.fn()
				.mockReturnValue("encrypted password");
			mockAuthenticationController.createTokenFromUser = jest
				.fn()
				.mockReturnValue("Some token");

			await mockAuthenticationController.handleRegister(
				mockRequest,
				mockResponse,
				null
			);

			expect(mockResponse.status).toHaveBeenCalledWith(201);
			expect(mockResponse.json).toHaveBeenCalledWith({
				accessToken: expect.any(String),
			});
		});

		it("should show error page if there's caught error", async () => {
			const mockNext = jest.fn();

			const mockAuthenticationController = new AuthenticationController({
				userModel: null,
			});

			await mockAuthenticationController.handleRegister(
				mockRequest,
				mockResponse,
				mockNext
			);
			expect(mockNext).toHaveBeenCalled();
		});
	});

	describe("handleGetUser", () => {
		let mockRequest, mockResponse;

		beforeEach(() => {
			mockRequest = {
				user: { id: 1 },
			};

			mockResponse = {
				status: jest.fn().mockReturnThis(),
				json: jest.fn().mockReturnThis(),
			};
		});

		it("should return user if success", async () => {
			const mockUserModel = {
				findByPk: jest.fn().mockReturnValue({
					roleId: 1,
					name: "Admin",
				}),
			};

			const mockRoleModel = {
				findByPk: jest.fn().mockReturnValue({
					id: 1,
					name: "ADMIN",
				}),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
				roleModel: mockRoleModel,
				bcrypt,
			});

			await mockAuthenticationController.handleGetUser(
				mockRequest,
				mockResponse
			);
			expect(mockResponse.status).toHaveBeenCalledWith(200);
			expect(mockResponse.json).toHaveBeenCalledWith({
				roleId: 1,
				name: "Admin",
			});
		});

		it("should return 404 if user not found", async () => {
			const mockUserModel = {
				findByPk: jest.fn().mockReturnValue(null),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
				bcrypt,
			});

			await mockAuthenticationController.handleGetUser(
				mockRequest,
				mockResponse
			);

			expect(mockResponse.status).toHaveBeenCalledWith(404);
			expect(mockResponse.json).toHaveBeenCalled();
		});

		it("should return 404 if role is not found", async () => {
			const mockUserModel = {
				findByPk: jest.fn().mockReturnValue({
					roleId: 1,
					name: "Admin",
				}),
			};

			const mockRoleModel = {
				findByPk: jest.fn().mockReturnValue(null),
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: mockUserModel,
				roleModel: mockRoleModel,
				bcrypt,
			});

			await mockAuthenticationController.handleGetUser(
				mockRequest,
				mockResponse
			);
			expect(mockResponse.status).toHaveBeenCalledWith(404);
			expect(mockResponse.json).toHaveBeenCalled();
		});
	});

	describe("JWT Encode/Decode", () => {
		let token;

		it("Should encode JWT token", () => {
			const mockUser = {
				id: 1,
				name: "Admin",
				email: "test@test.com",
				image: "some image",
			};

			const mockRole = {
				id: 1,
				name: "ADMIN",
			};

			const mockAuthenticationController = new AuthenticationController({
				userModel: null,
                jwt
			});

            token = mockAuthenticationController.createTokenFromUser(mockUser, mockRole);
            expect(token).toBeTruthy();
            expect(token).toEqual(expect.any(String));
		});

        it("Should decode JWT token", () => {

            const mockAuthenticationController = new AuthenticationController({
                userModel: null,
                jwt
            });

            const decoded = mockAuthenticationController.decodeToken(token);
            expect(decoded).toBeTruthy();
            expect(decoded).toEqual(expect.any(Object));
            expect(decoded.name).toEqual("Admin");
        });
	});

    describe("Encrypt/Verivy Password", () => {

        let hash;

        it("Should encrypt password", () => {
            const mockAuthenticationController = new AuthenticationController({
                userModel: null,
                bcrypt
            });

            hash = mockAuthenticationController.encryptPassword("password");
            expect(hash).toBeTruthy();
            expect(hash).toEqual(expect.any(String));
        });

        it("Should decrypt password", () => {
            const mockAuthenticationController = new AuthenticationController({
                userModel: null,
                bcrypt
            });

            const isValid = mockAuthenticationController.verifyPassword("password", hash);
            expect(isValid).toBeTruthy();
        });
    })
});
