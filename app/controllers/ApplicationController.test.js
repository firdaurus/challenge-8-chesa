const ApplicationController = require("./ApplicationController");

describe("ApplicationController", () => {
	describe("handleGetRoot", () => {
		it("should return a 200 status code", async () => {
			const mockRequest = {};

			const mockResponse = {
				status: jest.fn().mockReturnThis(),
				json: jest.fn().mockReturnThis(),
			};

			const appController = new ApplicationController();

			appController.handleGetRoot(mockRequest, mockResponse);
			expect(mockResponse.status).toHaveBeenCalledWith(200);
			expect(mockResponse.json).toHaveBeenCalledWith({
				status: "OK",
				message: "BCR API is up and running!",
			});
		});
	});

    describe("handleNotFound", () => {
        it("should return a 404 status code", () => {

            const mockRequest = {
                method: "GET",
                url: "/api/v1/users",
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            const appController = new ApplicationController();
            appController.handleNotFound(mockRequest, mockResponse);
            expect(mockResponse.status).toHaveBeenCalledWith(404);
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: "Error",
                    message: "Not found!",
                    details: {
                        method: "GET",
                        url: "/api/v1/users",
                    },
                }
            });
        })
    });

    describe("handleError", () => {
        it("Should return a 500 status code", () => {
            const mockRequest = {};
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            const appController = new ApplicationController();
            appController.handleError(new Error("Error"), mockRequest, mockResponse, null);

            expect(mockResponse.status).toHaveBeenCalledWith(500);
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: "Error",
                    message: "Error",
                    details: null,
                }
            });
        })
    });

    describe("getOffsetFromRequest", () => {
        it("Should return the offset", () => {
            const mockRequest = {
                query: {
                    page: 1,
                    pageSize: 10,
                }
            };

            const appController = new ApplicationController();
            const offset = appController.getOffsetFromRequest(mockRequest);

            expect(offset).toEqual(0);
        })
    });

    describe("buildPaginationObject", () => {
        it("Should return the pagination object", () => {
            const mockRequest = {
                query: {
                    page: 1,
                    pageSize: 10,
                }
            };

            const mockCount = 100;
            const paginationObject = (new ApplicationController).buildPaginationObject(mockRequest, mockCount);
            expect(paginationObject.count).toEqual(mockCount);
            expect(paginationObject.page).toEqual(1);
            expect(paginationObject.pageCount).toEqual(10);
            expect(paginationObject.pageSize).toEqual(10);
        });
    });
});
