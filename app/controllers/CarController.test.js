const CarController = require("./CarController");
const dayjs = require("dayjs");

describe("CarController", () => {
	describe("handleListCars", () => {
		it("should return a list of cars", async () => {
			const mockRequest = {
				query: {
					page: 1,
					pageSize: 10,
				},
			};

			const mockResponse = {
				status: jest.fn().mockReturnThis(),
				json: jest.fn().mockReturnThis(),
			};

			const mockCarModel = {
				findAll: jest
					.fn()
					.mockReturnValue(Promise.resolve(["cars1", "cars2"])),
				count: jest.fn().mockReturnValue(Promise.resolve(2)),
			};

			const carController = new CarController({
				carModel: mockCarModel,
			});

			await carController.handleListCars(mockRequest, mockResponse);
			expect(mockResponse.status).toHaveBeenCalledWith(200);
			expect(mockResponse.json).toHaveBeenCalledWith({
				cars: ["cars1", "cars2"],
				meta: expect.objectContaining({
					pagination: expect.objectContaining({
						page: 1,
						pageCount: 1,
						pageSize: 10,
						count: 2,
					}),
				}),
			});
		});
	});

	describe("handleGetCar", () => {
		it("should return a car", async () => {
			const mockRequest = {
				params: {
					id: 1,
				},
			};

			const mockResponse = {
				status: jest.fn().mockReturnThis(),
				json: jest.fn().mockReturnThis(),
			};

			const mockCar = {
				id: 1,
				name: "car1",
				price: 100,
			};

			const mockCarModel = {
				findByPk: jest.fn().mockReturnValue(Promise.resolve(mockCar)),
			};

			const carController = new CarController({
				carModel: mockCarModel,
			});

			await carController.handleGetCar(mockRequest, mockResponse);
			expect(mockResponse.status).toHaveBeenCalledWith(200);
			expect(mockResponse.json).toHaveBeenCalledWith(mockCar);
		});
	});

	describe("handleCreateCar", () => {
		let mockRequest, mockResponse;

		beforeEach(() => {
			mockRequest = {
				body: {
					name: "car1",
					price: 100,
					size: "small",
					image: "image1",
				},
			};

			mockResponse = {
				status: jest.fn().mockReturnThis(),
				json: jest.fn().mockReturnThis(),
			};
		});

		it("should return 201 status if success", async () => {
			const mockCarModel = {
				create: jest
					.fn()
					.mockReturnValue(Promise.resolve(mockRequest.body)),
			};

			const carController = new CarController({
				carModel: mockCarModel,
			});

			await carController.handleCreateCar(mockRequest, mockResponse);
			expect(mockResponse.status).toHaveBeenCalledWith(201);
			expect(mockResponse.json).toHaveBeenCalledWith(mockRequest.body);
		});

		it("should return 422 status if error", async () => {
			const mockCarModel = {
				create: jest.fn().mockReturnValue(
					Promise.reject({
						name: "ValidationError",
						message: "Validation failed",
					})
				),
			};

			const carController = new CarController({
				carModel: mockCarModel,
			});

			await carController.handleCreateCar(mockRequest, mockResponse);
			expect(mockResponse.status).toHaveBeenCalledWith(422);
			expect(mockResponse.json).toHaveBeenCalledWith({
				error: {
					name: "ValidationError",
					message: "Validation failed",
				},
			});
		});
	});

	describe("handleRentCar", () => {
		let mockRequest, mockResponse, mockNext;

		beforeEach(() => {
			mockRequest = {
				params: {
					id: 1,
				},
				body: {
					rentStartedAt: "2022-06-10T05:33:38.374Z",
				},
                user: {
                    id: 1,
                }
			};

            mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            mockNext = jest.fn();
		});

		it("should return 201 status if success", async () => {

            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(Promise.resolve({
                    id: 1,
                    name: "car1",
                    price: 100,
                    size: "small",
                    image: "image1",
                })),
            }

            const mockUserCarModel = {
                findOne: jest.fn().mockReturnValue(Promise.resolve(null)),
                create: jest.fn().mockReturnValue(Promise.resolve({
                    id: 1,
                    userId: 1,
                    carId: 1,
                    rentStartedAt: "2022-06-10T05:33:38.374Z",
                    rentEndedAt: "2022-06-10T05:33:38.374Z",
                })),
            }

            const carController = new CarController({
                carModel: mockCarModel,
                userCarModel: mockUserCarModel,
                dayjs
            })

            await carController.handleRentCar(mockRequest, mockResponse, mockNext);
            expect(mockNext).not.toHaveBeenCalled();
            expect(mockResponse.status).toHaveBeenCalledWith(201);
        });

		it("should return 422 status if already rented", async () => {
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(Promise.resolve({
                    id: 1,
                    name: "car1",
                    price: 100,
                    size: "small",
                    image: "image1",
                })),
            }

            const mockUserCarModel = {
                findOne: jest.fn().mockReturnValue(Promise.resolve(true)),
                create: jest.fn().mockReturnValue(Promise.resolve({
                    id: 1,
                    userId: 1,
                    carId: 1,
                    rentStartedAt: "2022-06-10T05:33:38.374Z",
                    rentEndedAt: "2022-06-10T05:33:38.374Z",
                })),
            }

            const carController = new CarController({
                carModel: mockCarModel,
                userCarModel: mockUserCarModel,
                dayjs
            })

            await carController.handleRentCar(mockRequest, mockResponse, mockNext);
            expect(mockResponse.status).toHaveBeenCalledWith(422);
            expect(mockResponse.json).toHaveBeenCalled();
        });

		it("should thrown to error page if error", async () => {
            const carController = new CarController({})

            await carController.handleRentCar(mockRequest, mockResponse, mockNext);

            expect(mockNext).toHaveBeenCalled();
        });
	});

	describe("handleUpdateCar", () => {

        let mockRequest, mockResponse;

        beforeEach(() => {
            mockRequest = {
                body: {
                    name: "car1",
                    price: 100,
                    size: "small",
                    image: "image1",
                },
                params: {
                    id: 1,
                },
            }

            mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }
        })

		it("should return 200 status if success", async () => {
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(Promise.resolve({
                    id: 1,
                    name: "car1",
                    price: 100,
                    size: "small",
                    image: "image1",
                    update: jest.fn().mockReturnValue(Promise.resolve(mockRequest.body)),
                })),
            }

            const carController = new CarController({
                carModel: mockCarModel,
                dayjs
            })

            await carController.handleUpdateCar(mockRequest, mockResponse);
            expect(mockResponse.status).toHaveBeenCalledWith(200);
            expect(mockResponse.json).toHaveBeenCalled();
        });

		it("should return 422 status if error", async () => {
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(Promise.resolve({
                    id: 1,
                    name: "car1",
                    price: 100,
                    size: "small",
                    image: "image1",
                    update: jest.fn().mockReturnValue(Promise.reject({
                        name: "ValidationError",
                    })),
                })),
            }

            const carController = new CarController({
                carModel: mockCarModel,
                dayjs
            })

            await carController.handleUpdateCar(mockRequest, mockResponse);
            expect(mockResponse.status).toHaveBeenCalledWith(422);
            expect(mockResponse.json).toHaveBeenCalled();
        });
	});

	describe("handleDeleteCar", () => {
		it("should return 204 status if success", async () => {

            const mockRequest = {
                params: {
                    id: 1,
                },
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                end: jest.fn(),
            }

            const mockCarModel = {
                destroy: jest.fn().mockReturnValue(Promise.resolve()),
            }

            const carController = new CarController({
                carModel: mockCarModel,
            });

            await carController.handleDeleteCar(mockRequest, mockResponse);
            expect(mockResponse.status).toHaveBeenCalledWith(204);
        });
	});

	describe("getCarFromRequest", () => {
		it("should return a car", () => {
            const mockRequest = {
                params: { id: 1}
            }

            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(Promise.resolve('car1'))
            };

            const carController = new CarController({
                carModel: mockCarModel})

            carController.getCarFromRequest(mockRequest)
            expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
        });
	});

	describe("getListQueryFromRequest", () => {
		it("should return a query", () => {
            const mockRequest = {
                query: {
                    page: 1,
                    size: 10,
                    sort: "name",
                    order: "asc",
                    availableAt: "2020-06-10T05:33:38.374Z",
                }
            }

            const carController = new CarController({})

            const query = carController.getListQueryFromRequest(mockRequest)
            expect(query).toHaveProperty("offset", 0);
        });
	});
});
